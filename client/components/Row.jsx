import React from 'react';
import Square from './Square.jsx';

const Row = ({ row, startGame, currentPlayer }) => {
  return(
    <tr>
      {row.map((square, index) =>
      <Square key={index} column={index} value={square} startGame={startGame} />
      )}
    </tr>
  );
};

export default Row;